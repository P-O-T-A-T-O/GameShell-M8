# StopGap

I intend to update this with the full directions on the steps I followed to get the gameshell to communicate with the teensy 4.1 and customized the OS to operate as an appliance.

In the meantime, in the interest of making this easy to deploy, this document will serve as a stopgap with a copy of my current image and my current customizations so you could just write it to an SD card (16GB or greater) and hit the races.

## Attribution

This lil' setup is built on the backs of giants and implemented by a hack!

- Trash80

  Without him none of this would be possible. The m8 is great hardware, heads and tails above this hack, and the amount of support he's provided to the community even in the headless world is outstanding.

  I would highly encourage supporting the real hardware : https://dirtywave.com

- m8c

  This is the awesome and simple client we use to communicate with the m8 flashed teensy 4.1: https://github.com/laamaa/m8c

  This has been patched to accommodate the lower resolution display of the gameshell: https://codeberg.org/P-O-T-A-T-O/m8c-gameshell-git

- Gameshell Arch Linux port: https://forum.clockworkpi.com/t/os-arch-linux/5009

  Kernel has been recompiled with support for USB ACM, forked from r043v's PKGBUILDs:

  - Original: https://github.com/r043v/GameShell-PKGBUILDs

  - Modified for USB ACM support(Compiled package in releases page): https://codeberg.org/P-O-T-A-T-O/linux-gameshell

## Hardware Requirements

Gameshell from clockwork pi: https://www.clockworkpi.com/gameshell

Teensy 4.1 development board: https://www.pjrc.com/store/teensy41.html

USB Micro to Micro OTG: https://www.amazon.com/gp/product/B0744BW2B2

Any USB Micro to Micro OTG cable should work, but the one I used is the one I linked above.

Minimum 16GB Mini SD card for the gameshell and your choice of a high speed SD card for the Teensy 4.1

## Pre-requisites:

Follow the directions hosted here for creating a headless M8 using the teensy 4.1: https://github.com/DirtyWave/M8Docs/blob/main/docs/M8HeadlessSetup.md

## Usage

- Download image: https://publicshare.us-southeast-1.linodeobjects.com/Gameshell-M8.img.tgz

- Unpack image: `tar -xvzf Gameshell-M8.img.tgz`

- Verify image integrity: `md5sum -c Gameshell-M8.img.md5`

- Using your favorite image writing software, write the image to an SD card and boot the gameshell.

  - On linux, it's simply `sudo dd if=Gameshell-M8.img of=/dev/<SDCard> bs=1M status=progress`

- Connect teensy 4.1 via USB OTG cable.

- Start M8 via `Menu` -> `Launch`

- Use bindings listed below:

### Bindings - Global

All global keybindings can be found in `/home/m8/.config/sway/config`

|             Definition              |  Gameshell Button  |
| :---------------------------------: | :----------------: |
|              Volume Up              | `Shift` + `Start`  |
|             Volume Down             | `Shift` + `Select` |
|       Start/Stop Menu for m8c       |       `Menu`       |
| System Menu (Power and Reload sway) |  `Shift` + `Menu`  |
|      Increase Screen Backlight      |        `Y`         |
|      Decrease Screen Backlight      |   `Shift` + `Y`    |

### Bindings - m8c

All m8c keybindings can be found in `/home/m8/.config/m8c/config.ini`

Note: Not all keys are bound by default.

|   M8 Key   | Gameshell Button |
| :--------: | :--------------: |
|     Up     |    `D-Pad Up`    |
|    Left    |   `D-Pad Left`   |
|    Down    |   `D-Pad Down`   |
|   Right    |  `D-Pad Right`   |
|   Select   |     `Select`     |
| Select Alt |    Not bound     |
|   Start    |     `Start`      |
| Start Alt  |    Not bound     |
|    Opt     |  `Shift` + `B`   |
|  Opt Alt   |       `B`        |
|    Edit    |  `Shift` + `A`   |
|  Edit Alt  |       `A`        |
|   Delete   |       `X`        |
|   Reset    |  `Shift` + `X`   |

## Additional Information

### SSH access

When the gameshell is connected to a laptop via the USB micro cable, a DHCP server will be setup and it will be seen as a network interface.

You can ssh into the gameshell when connected via the USB using `ssh m8@192.168.0.1`.

The user m8 has sudo permission, no password, and ssh is setup to allow empty passwords.

### Auto-shutdown

On startup swayidle is started and set to a 10 minute timeout. If there is no activity locally after 10 minutes, it will poweroff.

This is started in `/home/m8/.config/sway/config`. If you're doing work via SSH this can be annoying. You'll want to run `pkill -fi swayidle` after you ssh in to prevent it from powering down.

### Invoking things in gameshell session via ssh

When ssh'd into the gameshell, you can export the environment variables in `/home/m8/.swayenv` by looping over them quickly:

`for i in $(cat /home/m8/.swayenv); do export "$i"; done`

This will allow you to adjust volume and interact with the swaysession on screen via SSH if you're working on things.

### Basic information on what's on this image:

Sway is used as the window manager and is autostarted via the systemd service file, `sway@tty1.service`

Swaynagmode, https://github.com/b0o/swaynagmode , is used for the menu options to start m8c and for the power options.

m8c is started up via a systemd service that first wires the audio I/O of the teensy to the audio I/O of the gameshell. Upon shutdown it dewires it as well.

I intend to update this repo with files and better instructions on how to build your own images later on.

## Additional notes

I have wifi and bluetooth all disabled in the interest of battery life.

The status bar below is set to only update every 2 seconds in the interest of battery life and the fact I am just using waybar and a bash script. You'll notice the delay when changing the volume frequently.

Battery life is _ok_ at best and I would recommend upgrading your battery. There are a lot of ways to do that and I won't go into that here.

There are a lot of aspects that are hacky as it wasn't really intended to be distributed and just used while I waited for my own M8.

In other words, I'm sure there's plenty to be improved upon and feel free to do so!
